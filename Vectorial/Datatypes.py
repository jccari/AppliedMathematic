import numpy as np

class VectorialSpace:
    def __init__(self, _value):
        #print " > Calling VectorialSpace class"
        self.value = _value

    def __add__(self, other):
        raise NotImplementedError("[ERROR] Not Implemented")

    def __rmul__(self, other):
        raise NotImplementedError("[ERROR] Not Implemented")


class Vector(VectorialSpace):

    def __str__(self):
        return "%s" %(self.value,)  # is a tuple that represent a R-n dimensional Vector

    def vector_sum_vector(self, other_vector):
        # if ( len(self.value) == len(other_vector.value)): raise #SomeError
        result = ()
        for i in range(0, len(other_vector.value)):
            sum = self.value[i] + other_vector.value[i]
            result = result + (sum,)
        return result

    def alpha_mult_vector(self, alpha):
        result = ()
        for i in range(0, len(self.value)):
            mult = alpha * self.value[i]
            result = result + (mult,)
        return result

    def __rmul__(self, other):  ## alpha*vector
        """
        Overloading * (multiply) operator (reverse form)
        Multiply alpha integer and Vector self.value
        Args:
            other: is a integer

        Returns: Vector result alpha*Vector
        """
        try:
            if type(other) == int:
                #print "[OK] alpha_mult_vector"
                return self.alpha_mult_vector(other)
        except:
            print "[ERROR] not match"

    def __add__(self, other):
        """
        Overloading + (plus) operator
        Add other.value (Vector) and self.value (Vector)
        Args:
            other: Object Vector

        Returns: Vector resultant of self.value + other.value

        """
        try:
            if type(other.value) == tuple: ## Vector + Vector
                #print "[OK] vector_sum_vector"
                result = self.vector_sum_vector(other)
                resultVect = Vector(result)
                return resultVect

        except:
            print "[ERROR] not match"


class Matrix(VectorialSpace):

    def matrix_to_str(self, matrix):
        m_str = "["
        size = len(matrix)
        for i in range(0, size):
            m_str += "["
            for j in range(0, size):
                m_str += str(matrix[i][j]) +","
            m_str = m_str[:len(m_str)-1]
            m_str += "],"

        m_str = m_str[:len(m_str)-1]
        m_str += "]"
        return m_str

    def __str__(self):
        #return self.value # Is a list of list that represent a Matrix
        #return ''.join("["+str(r)+"]" for v in self.value for r in v)
        return self.matrix_to_str(self.value)

    def matrix_sum_matrix(self, other_matrix):
        row = len(self.value)
        col = len(self.value[0])

        sum_matrix = [[0 for x in range(row)]for y in range(col)]

        for i in range(0,col):
            for j in range(0, row):
                sum_matrix[i][j] = self.value[i][j] + other_matrix[i][j]

        return sum_matrix

    def alpha_mult_matrix(self, alpha):
        row = len(self.value)
        col = len(self.value[0])

        mul = [[0 for x in range(row)]for y in range(col)]

        for i in range(0,col):
            for j in range(0, row):
                mul[i][j] = self.value[i][j] * alpha

        return mul

    def __rmul__(self, other):  ##  alpha * Matrix
        # print type(other.value)
        try:
            if type(other) == int:
                res_matrix = Matrix(self.alpha_mult_matrix(other))
                return res_matrix
                #print "[OK] alpha_mult_matrix"

        except:
            print "[ERROR] not match"

    def __add__(self, other): ## Matrix + Matrix
        try:
            if type(other.value) == list:
                #print "[OK] matrix_sum_matrix", other.value
                res_matrix = Matrix(self.matrix_sum_matrix(other.value))
                return res_matrix
        except:
            print "[ERROR] not match"


class Function(VectorialSpace):

    def __str__(self):
        return self.value  #is a str that represent a function

    def funct_sum_funct(self, other_function):
        return self.value + "+(" + other_function.value +")"

    def alpha_mult_funct(self, alpha):
        return str(alpha) + "*("+self.value+")"

    def __rmul__(self, other):  ## alpha*Function
        # print type(other.value)
        try:
            if type(other) == int:
                #print "[OK] alpha_mult_funct"
                res_funt = Function(self.alpha_mult_funct(other))
                return res_funt

        except:
            print "[ERROR] not match"

    def __add__(self, other):  ##
        try:
            if type(other.value) == str:  ## Function + Function
                #print "[OK] funct_sum_funct"
                res_funt = Function(self.funct_sum_funct(other))
                return res_funt

        except:
            print "[ERROR] not match"
