from expressions import *
from Datatypes import Vector, Matrix, Function

class AllowingCompiler(Compiler):
    def compile_literal(self, context, literal):
        return literal

    def compile_variable(self, context, variable):

        if context and variable.name not in context:
            raise Exception("Variable '%s' is not allowed" % variable)

        return variable

    def compile_binary(self, context, operator, op1, op2):
        return "(%s %s %s)" % (op1, operator, op2)

    def compile_function(self, context, function, args):
        arglist = ", " % args
        return "%s(%s)" % (function, arglist)


compiler = AllowingCompiler()

#result = compiler.compile("a*b", allowed_variables)
a = 3
exec("b = Vector((1,1))")
#c = Datatypes.Vector((1,1))

#result = compiler.compile("a*b")
#result = compiler.compile("b")
exec("c = Vector((1,1))")
result = "b+c"

#a = Datatypes.Vector((1,2))

#print("Result is {}".format(eval(result)))
#print type(eval(result))

def execute_cmd(cmd):
    try:
        if "=" in cmd:
            exec(cmd)
            print " [OK] Declaration variable succesfull"
        else:
            print " [OK] Evaluation succesfullaaaaaaaaaaa"
            result = cmd
            print "operation:",result
            print("Result is {}".format(eval(result)))
            print " [OK] Evaluation succesfull"
    except:
        print " [ERROR] Invalid Operation, Check your input please"
